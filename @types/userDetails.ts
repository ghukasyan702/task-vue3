interface Task {
    id:Number,
    title:String,
    description:String,
    created_at: null | Date,
}
export interface UsersDetails {
    id: Number
    name: String
    email: String
    email_verified_at: null | Date
    created_at: null | Date
    tasks: Task[]
}