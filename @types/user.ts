export interface Users {
    id: Number
    name: String
    email: String
    email_verified_at: null | Date
    created_at: null | Date
}