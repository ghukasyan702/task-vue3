export interface Tasks {
    id:Number,
    user:Object,
    title:String,
    description:String,
    status:Number,
    created_at: null | Date,
}