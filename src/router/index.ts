import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Home from "../views/Home.vue";
import Tasks from "../views/Tasks.vue";
import Users from "../views/Users.vue";

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/tasks',
        name: 'Tasks',
        component: Tasks
    },
    {
        path: '/users',
        name: 'Users',
        component: Users
    }
];

const router = createRouter({
    history: createWebHistory(''),
    routes
});

export default router;
